import React from 'react';
import Button from '../Button';
import Display from './Display';
import LightsPanel from '../LightsPanel';
import LightStatus from '../LightsPanel/LightStatus';
import { codes } from './codes';

const defaultLights = [LightStatus.OFF, LightStatus.OFF, LightStatus.OFF]

export default class Controls extends React.Component {

    state = {
        lights: defaultLights.slice(),
        crtLight: 0
    }

    render() {
        return (
            <div className="controls">
                <Display digits={this.props.digits} />
                <Button className="red" label="CLEAR" onClick={this.props.onClear} />
                <Button className="green" label="ENTER" onClick={this.submitCode} />
                <LightsPanel lights={this.state.lights} />
            </div>
        )
    }

    submitCode = () => {
        // don't post the code on insufficient digits or when all 3 lights are on
        const digitsAsString = "" + this.props.digits;
        if (digitsAsString.length < 3 || this.state.crtLight === 3) {
            return;
        }
        const status = digitsAsString === codes[this.state.crtLight] ? LightStatus.GREEN : LightStatus.RED;
        const newLights = this.state.lights.slice();
        newLights[this.state.crtLight] = status;
        this.setState({ lights: newLights, crtLight: this.state.crtLight + 1 },
            this.checkAllCodesInserted);
        this.props.onClear();
    }

    checkAllCodesInserted = () => {
        if (this.state.crtLight === 3) {
            // If there were flags != green then reset after 3 sec, otherwise reset after 5 sec
            const delay = this.state.lights.filter(light => light !== LightStatus.GREEN).length ? 3000 : 5000;
            console.log(`Clearing lights after ${delay} millis.`);
            setTimeout(this.clearLights, delay);
        }
    }

    clearLights = () => {
        this.setState({
            lights: defaultLights.slice(),
            crtLight: 0
        });
    }
}