import React from 'react';

// This sfc was defined as a subcomponent of Controls as an example, there's really no difference 
// to declaring it in a higher scope, other than keeping components that are unlikely to be used outside the greater scope.

const Display = (props) =>
    <div className="button display">{print(props.digits)}</div>

const print = (digits = "") => {
    // Only print 3 digits
    digits = ("" + digits).substr(0, 3);
    // Pad the number with "_" to the right, up to 3 digits
    return digits + '_'.repeat(3 - digits.length);
}

export default Display;