import React from 'react';

export const Key = (props) =>
    <div className="button number-key" onClick={() => props.onKeyPress(props.label)}>{props.label}</div>