import React from 'react';

const Button = (props) =>
    <div className={`button command-key ${props.className}`} onClick={props.onClick}>{props.label}</div>

export default Button;