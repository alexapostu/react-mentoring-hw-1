import getUuid from 'uuid';

// Overkill but funny - generate an array containing 1-9, "", "0", ""
export const [, ...keyDigits] = [...Array(10).keys(), "", "0", ""];
// Generating IDs outside the render function ensures they remain consistent between renders.
export const keyUuids = keyDigits.map(() => getUuid());