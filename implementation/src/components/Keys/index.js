import React from 'react';
import { Key } from '../Key';
import { keyDigits, keyUuids } from './constants';

const Keys = (props) => (
    <div className="keys">
        {keyDigits.map((keyVal, i) =>
            <Key
                key={keyUuids[i]}
                label={keyVal ? keyVal : "\xA0"}
                onKeyPress={keyVal ? props.onKeyPress : () => { }} />)}
    </div>
)
export default Keys;