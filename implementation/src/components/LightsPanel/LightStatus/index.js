const LightStatus = Object.freeze({
    OFF:   Symbol("off"),
    RED:  Symbol("red"),
    GREEN: Symbol("green")
});

export default LightStatus;