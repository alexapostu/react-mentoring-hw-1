import React from 'react';
import LightStatus from './LightStatus'

const lightClasses = {
    [LightStatus.OFF]: '',
    [LightStatus.RED]: 'red',
    [LightStatus.GREEN]: 'green',
}

const LightsPanel = (props) => (
    <div className="lights">
        {props.lights.map((light, i) =>
            <React.Fragment key={i}>
                <br />
                <div className={`light ${lightClasses[light]}`}>&nbsp;</div>
            </React.Fragment>)}
    </div>
)

export default LightsPanel;