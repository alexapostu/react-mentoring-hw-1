import React from 'react';
import Keys from './components/Keys';
import Controls from './components/Controls';
import './App.css';

export default class App extends React.Component {

  state = {
    combination: ""
  }

  render() {
    return (
      <div className="keypad">
        <Keys onKeyPress={this.digitPressed} />
        <Controls
          digits={this.state.combination}
          onClear={this.onClear}
          onEnter={this.onEnter} />
      </div>
    )
  }

  digitPressed = (value) => {
    if (this.state.combination.length < 3) {
      this.setState({ combination: this.state.combination + value });
    }
  }

  onClear = () => {
    console.log('Clearing display.');
    this.setState({ combination: "" })
  }
}